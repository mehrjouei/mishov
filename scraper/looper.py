from lib.httpRequests import simple_get
from bs4 import BeautifulSoup
import lib.crawler as crawler
import time
import random

# get list
url = "http://codal.ir/ReportList.aspx?1=&2=&3=&4=58&5=&6=&7=&8=-1&9=-1&10=-1&11=&12=False&13=0&15=-1&16=-1&17=-1&18="
# list_html = simple_get(url)
# startFromFirst = False
# listPage = BeautifulSoup(list_html, 'html.parser')
counter = 1

while True:
    startFromFirst = False
    try:
        if counter==1:
            list_html = simple_get(url)
            listPage = BeautifulSoup(list_html, 'html.parser')

        print('################'+str(counter)+'##############')
        list = listPage.select('.ReportListGrid.breakeWord tr')
        saveFlag = "none"
        for row_counter in range(1, len(list)-1):
            try:
                saveFlag = crawler.loadAndSaveContentOfThisRow(list[row_counter])
                if saveFlag == "Exist":
                        startFromFirst = True
                        print("Exist")
                        break
                elif saveFlag == "unknown":
                    print("record unknown")
                elif saveFlag == "error":
                    print("record Save Error")
                else:
                    print("record saved")
            except:
                print("error on save record")

        if startFromFirst:
            counter=1
            print("startFromFirst")
            time.sleep(random.randint(60,160))
            print("spleeped")
            continue
            
        print("getNextPage")
        nextPage = crawler.HtmlOfNextPageOfThisPage(listPage, url,counter)
        if nextPage["status"] == "ok":
            listPage=nextPage["data"]
        else:
            print("error on get")
            break
        if counter == 150:
            break
        counter += 1
    except:
            print("outer Error")
