from lib.httpRequests import simple_get, simple_post
import lib.jalali as jalali
from bs4 import BeautifulSoup
from requests import post
import json


def loadAndSaveContentOfThisRow(row_html):
    # findLinkAndLoadPage:
    aTag = row_html.select('.HTMLIcon')
    row_title = row_html.select(".URLTitle span")[0].text
    pchYear = (row_html.select(".DateTitle")[0].text.split(" ")[0])
    crYear = str(int(pchYear.split(
        "/")[0]))+"/"+str(int(pchYear.split("/")[1]))+"/"+str(int(pchYear.split("/")[2]))
    crTimeP = row_html.select(".DateTitle")[0].text.split(
        " ")[1].replace('\r', '').replace('\n', '').replace('\t', '')
    crTime = str(int(crTimeP.split(":")[
                 0]))+":"+str(int(crTimeP.split(":")[1]))+":"+str(int(crTimeP.split(":")[2]))
    row_CreateTime = jalali.Persian(crYear).gregorian_string(
    )+" "+crTime
    try:
        page_html = simple_get("http://codal.ir/"+aTag[0].attrs['href'])
        page = BeautifulSoup(page_html, 'html.parser')
        values = getPageValuesJson(page)
        values["monthlyReportTitle"] = row_title.replace(
            '\r', '').replace('\n', '').replace('\t', '')
        values["CreateTime"] = row_CreateTime
        return saveValues(values)
    except:
        return "error"


def HtmlOfNextPageOfThisPage(page_html, url,currentPage,moveDirection="next"):
    try:

        __VIEWSTATE = page_html.select("#__VIEWSTATE")[0].attrs['value']
        rowIndex = 20*(currentPage)
        if moveDirection=="next":
            nextpage_html = simple_post(url,
                                    {
                                        'ctl00$ContentPlaceHolder1$ucPager1$btnNext': '',
                                        'ctl00$ContentPlaceHolder1$ucPager1$hdfFromRowIndex': rowIndex,
                                        'ctl00$ContentPlaceHolder1$ucPager1$hdfCurrentGroup': 1,
                                        'ctl00$ContentPlaceHolder1$ucPager1$hdfNavigatorIndex': currentPage+1,
                                        '__VIEWSTATE': __VIEWSTATE,
                                    })
        elif moveDirection=="prev":
            nextpage_html = simple_post(url,
                                    {
                                        'ctl00$ContentPlaceHolder1$ucPager1$btnBack': '',
                                        'ctl00$ContentPlaceHolder1$ucPager1$hdfFromRowIndex': rowIndex,
                                        'ctl00$ContentPlaceHolder1$ucPager1$hdfCurrentGroup': 1,
                                        'ctl00$ContentPlaceHolder1$ucPager1$hdfNavigatorIndex': currentPage-1,
                                        '__VIEWSTATE': __VIEWSTATE,
                                    })
        return {"data": BeautifulSoup(nextpage_html, 'html.parser'), "status": "ok"}
    except :
        return {"status": "error"}


def getPageValuesJson(page):
    retValue = {}
    retValue["Name"] = page.select("#ctl00_txbSymbol")[0].text
    if len(page.select("#ctl00_lblDisplaySymbol")) > 0:
        symbolName = page.select("#ctl00_lblDisplaySymbol")[0].text.replace(
            '(', '').replace(')', '').replace(' ', '')
        if len(symbolName) > 0:
            retValue["Name"] = page.select("#ctl00_lblDisplaySymbol")[
                0].text.replace('(', '').replace(')', '')
    retValue["CompanyName"] = page.select("#ctl00_txbCompanyName")[0].text
    retValue["ISICode"] = page.select("#ctl00_lblISIC")[0].text
    retValue["Fund"] = page.select("#ctl00_lblListedCapital")[
        0].text.replace(',', '')
    dataString = page.select("#ctl00_lblPeriodEndToDate bdo")[0].text
    yearEndString = page.select("#ctl00_lblYearEndToDate bdo")[0].text
    if len(page.select("#txtCorrectionComment")) > 0:
        retValue["Description"] = page.select("#txtCorrectionComment")[0].text
    else:
        retValue["Description"] = ""
    retValue["AccountingPeriod"] = jalali.Persian(
        yearEndString).gregorian_string()
    retValue["ReportDate"] = jalali.Persian(dataString).gregorian_string()
    if len(page.select("#ctl00_cphBody_ucProduct2_dgProduction tr")) > 0:
        tableLastRow = page.select(
            "#ctl00_cphBody_ucProduct2_dgProduction tr")[-1].select("td")
        if len(tableLastRow) == 21:
            retValue["CurrentReportSum"] = int(
                tableLastRow[16].text.replace(',', ''))
            retValue["SumFromFirst"] = int(
                tableLastRow[20].text.replace(',', ''))
            retValue["error"] = ""
            retValue["CompanyType"] = "PR"
            return retValue
    elif len(page.select("#ctl00_cphBody_ucProduct1_Table1 tr"))> 0:
        tableLastRow = page.select(
            "#ctl00_cphBody_ucProduct1_Table1 tr")[-1].select("td")
        if len(tableLastRow) == 10:
            retValue["CurrentReportSum"] = int(
                tableLastRow[5].text.replace(',', ''))
            retValue["SumFromFirst"] = int(tableLastRow[9].text.replace(',', ''))
            retValue["error"] = ""
            retValue["CompanyType"] = "PR"
            return retValue
    
    retValue["error"] = "not good formated"
    retValue["CompanyType"] = "UN"

    return retValue


def saveValues(values):
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    print(values["Name"])
    ret = post("http://127.0.0.1:8000/importer/monthlyReport",
               data=json.dumps(values), headers=headers)

    if ret.status_code==200:
        return ret.text
    else:
        return "error"