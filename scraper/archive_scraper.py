from lib.httpRequests import simple_get
from bs4 import BeautifulSoup
import lib.crawler as crawler

# get list
url = "http://codal.ir/ReportList.aspx?1=&2=&3=&4=58&5=&6=&7=&8=-1&9=-1&10=-1&11=&12=False&13=0&15=-1&16=-1&17=-1&18="
list_html = simple_get(url)
breakWhile = False
listPage = BeautifulSoup(list_html, 'html.parser')
counter = 1
#loop to reach to counter
for index in range(1,120):
    nextPage = crawler.HtmlOfNextPageOfThisPage(listPage, url,counter,"next")
    if nextPage["status"] == "ok":
        listPage=nextPage["data"]
        print (counter)
        counter+=1
    else:
        print("error on get")
        break

while not breakWhile:
    try:
        print('################'+str(counter)+'##############')
        list = listPage.select('.ReportListGrid.breakeWord tr')
        saveFlag = "none"
        for row_counter in range(1, len(list)-1):
            try:
                saveFlag = crawler.loadAndSaveContentOfThisRow(list[row_counter])
                if saveFlag == "replaced":
                    print("replaced")
                elif saveFlag == "done":
                    print("report saved")
                elif saveFlag == "exist":
                    print("report exist")
                elif saveFlag == "unknown":
                    print("report unknown Type")
            except:
                print("error on save record")

        prevPage = crawler.HtmlOfNextPageOfThisPage(listPage, url,counter,"prev")
        if prevPage["status"] == "ok":
            listPage=prevPage["data"]
        else:
            print("error on get")
            break
        if counter == 1:
            break
        counter -= 1
    except:
            print("outer Error")
