from django.db import models
from django.contrib import admin


class Ticker(models.Model):
    Name = models.CharField(max_length=100)
    CompanyName = models.CharField(max_length=255)
    ISICode = models.CharField(max_length=100)
    Fund = models.BigIntegerField(default=0)
    AccountingPeriod = models.DateField(null=True)
    CompanyType=models.CharField(max_length=2,default="")
    class Meta:
        db_table = "Ticker"
admin.site.register(Ticker)