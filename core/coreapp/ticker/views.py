from django.shortcuts import render,get_object_or_404
from .models import Ticker


def checkTickerExist(request):
    temp=Ticker.objects.filter(Name=request)
    if len(temp) ==0 :
        return False
    else:
        return True


def saveTicker(request):
    ticker=Ticker()
    ticker.Name=request["Name"]
    ticker.CompanyName=request["CompanyName"]
    ticker.Fund=request["Fund"]
    ticker.ISICode=request["ISICode"]
    ticker.AccountingPeriod=request["AccountingPeriod"]
    ticker.CompanyType=request["CompanyType"]
    ticker.save()
    return "done"