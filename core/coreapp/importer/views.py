from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.http import HttpResponse
import json
from ticker.views import checkTickerExist,saveTicker
from monthlyReports.views import checkMonthlyReportExist,saveMonthlyReport



@csrf_exempt
def monthlyReport(request):
    data = json.loads(request.body)
    if not(checkTickerExist(data["Name"])):
        saveTicker(data)

    if data["CompanyType"]=="PR":
        if not(checkMonthlyReportExist(data)):
            ret= saveMonthlyReport(data)
            return HttpResponse(ret)
        else:
            return HttpResponse("Exist")
    return HttpResponse("unknown")

