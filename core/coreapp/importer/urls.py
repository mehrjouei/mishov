from django.urls import path
from . import views


urlpatterns = [
    path('monthlyReport', views.monthlyReport),
]
