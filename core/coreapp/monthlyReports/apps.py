from django.apps import AppConfig


class MonthlyreportsConfig(AppConfig):
    name = 'monthlyReports'
