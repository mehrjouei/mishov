from django.db import models
from django.contrib import admin
from ticker.models import Ticker


class MonthlyReport(models.Model):
    Ticker = models.ForeignKey(Ticker, on_delete=models.PROTECT)
    CreateTime = models.DateTimeField(null=True)
    ReportDate = models.DateField(null=True)
    Title = models.CharField(max_length=255)
    Description = models.TextField()
    CurrentReportSum = models.BigIntegerField(default=0)
    PrevMonthsAverageSum = models.BigIntegerField(default=0)
    MonthNumberFromStart = models.IntegerField(default=0)
    PrevMonthReportSum = models.BigIntegerField(default=0)

    def __str__(self): 
        return str(self.Ticker.Name)+" "+str(self.ReportDate)
    class Meta:
        db_table = "MonthlyReport"


admin.site.register(MonthlyReport)
