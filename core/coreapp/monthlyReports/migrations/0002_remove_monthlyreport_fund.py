# Generated by Django 2.1.3 on 2018-11-19 20:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('monthlyReports', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='monthlyreport',
            name='Fund',
        ),
    ]
