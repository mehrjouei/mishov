from django.shortcuts import render
from datetime import datetime,timedelta

from .models import MonthlyReport
from ticker.models import Ticker
import requests
import urllib.parse

def checkMonthlyReportExist(request):
    temp = MonthlyReport.objects.filter(
        Ticker__Name=request["Name"], Title=request["monthlyReportTitle"], CreateTime=request["CreateTime"])
    if len(temp) == 0:
        return False
    else:
        return True


def saveMonthlyReport(request):
    sameReportArray = MonthlyReport.objects.filter(
        Ticker__Name=request["Name"], ReportDate=request["ReportDate"])
    if len(sameReportArray) > 0:
        sameReport = sameReportArray[0]
        sameReport.Title = request["monthlyReportTitle"]
        sameReport.CurrentReportSum = request["CurrentReportSum"]
        sameReport.Description = request["Description"]
        sameReport.CreateTime = request["CreateTime"]
        sameReport.ReportDate = request["ReportDate"]
        mReportDate = datetime.strptime(sameReport.ReportDate, '%Y-%m-%d')
        d = int((sameReport.Ticker.AccountingPeriod - mReportDate.date()).days)

        if d >= 365:
            d = d % 365

        daysPast = daysPast = 365-d
        monthsPast = round(float(daysPast)/30)
        sameReport.PrevMonthsAverageSum = round(
            request["SumFromFirst"]/monthsPast)
        sameReport.save()
        telegramMessage="اصلاحیه: \n"+"نام نماد: "+monthlyReport.Ticker.Name+"\n"+"نام شرکت: "+monthlyReport.Ticker.CompanyName+"\n"+"قروش این ماه شرکت: : "+str(monthlyReport.CurrentReportSum)+"\n"+"فروش ماه قبل: "+str(monthlyReport.PrevMonthReportSum)+"\n"+"فروش میانگین سال مالی: "+str(monthlyReport.PrevMonthsAverageSum)+"\n"
        proxies = {'http': "http://127.0.0.1:2080",'https': "https://127.0.0.1:2080"}
        url = 'https://api.telegram.org/bot719424258:AAG4ngwtfekirQLOy2ZHlNIrti3McxuGJcE/sendMessage?chat_id=-1001426696711&text='+urllib.parse.quote_plus(telegramMessage)
        requests.get(url, proxies=proxies)
        return "replaced"
    else:
        monthlyReport = MonthlyReport()
        ticker = Ticker.objects.get(Name=request["Name"])
        monthlyReport.Ticker = ticker
        monthlyReport.Title = request["monthlyReportTitle"]
        monthlyReport.CurrentReportSum = request["CurrentReportSum"]
        monthlyReport.Description = request["Description"]
        monthlyReport.CreateTime = request["CreateTime"]
        monthlyReport.ReportDate = request["ReportDate"]
        mReportDate = datetime.strptime(monthlyReport.ReportDate, '%Y-%m-%d')
        d = int((ticker.AccountingPeriod - mReportDate.date()).days)
        if d< 0:
            ticker.AccountingPeriod= request["AccountingPeriod"]
            ticker.save()
            d = int((ticker.AccountingPeriod - mReportDate.date()).days)
        if d >= 365:
            d = d % 365
        daysPast = 365-d
        monthsPast = round(float(daysPast)/30)
        prevMonthNumber=monthsPast-1
        if prevMonthNumber==0:
            prevMonthNumber=1
        
        enddate=datetime.strptime(monthlyReport.ReportDate, '%Y-%m-%d')
        startdate = enddate - timedelta(days=40)
        prevMonth=MonthlyReport.objects.filter(Ticker__Name=request["Name"],MonthNumberFromStart=prevMonthNumber,ReportDate__range=[startdate, enddate]).order_by('-ReportDate')
        if len(prevMonth)>0:
            monthlyReport.PrevMonthReportSum=prevMonth[0].CurrentReportSum
        else:
            monthlyReport.PrevMonthReportSum=0
        monthlyReport.MonthNumberFromStart = monthsPast
        monthlyReport.PrevMonthsAverageSum = round(request["SumFromFirst"]/monthsPast)
        monthlyReport.save()

        telegramMessage="نام نماد: "+monthlyReport.Ticker.Name+"\n"+"نام شرکت: "+monthlyReport.Ticker.CompanyName+"\n"+"قروش این ماه شرکت: : "+str(monthlyReport.CurrentReportSum)+"\n"+"فروش ماه قبل: "+str(monthlyReport.PrevMonthReportSum)+"\n"+"فروش میانگین سال مالی: "+str(monthlyReport.PrevMonthsAverageSum)+"\n"

        proxies = {'http': "http://127.0.0.1:2080",'https': "https://127.0.0.1:2080"}
        url = 'https://api.telegram.org/bot719424258:AAG4ngwtfekirQLOy2ZHlNIrti3McxuGJcE/sendMessage?chat_id=-1001426696711&text='+urllib.parse.quote_plus(telegramMessage)
        requests.get(url, proxies=proxies)
    return "done"
